# mypy typechecking modules
from typing import List
from typing import Tuple

# general modules
import glob
import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.constants import R
from IPython.display import display, Markdown, Latex

# alchemlyb parsing, preprocessing and FE estimators
import alchemlyb
from alchemlyb.parsing import amber
from alchemlyb.preprocessing.subsampling import slicing
from alchemlyb.estimators import TI, MBAR, AutoMBAR

# alchemlyb plotting
from alchemlyb.convergence import forward_backward_convergence
from alchemlyb.visualisation import (
    plot_convergence,
    plot_mbar_overlap_matrix,
    plot_ti_dhdl,
)
from alchemlyb.visualisation.dF_state import plot_dF_state


class AlchemicalAnalysis:
    def __init__(self, path: str, T_sim: float, cut_non_eq: int):
        self.files = self.get_files(path)
        self.temp = T_sim
        self.cut_non_eq = cut_non_eq
        self.dVdl_kJ = self.get_dVdl()
        self.dVdl_kcal = alchemlyb.postprocessors.units.to_kcalmol(self.dVdl_kJ)
        self.TI = self.get_TI()
        self._MBAR = None
        self.dG = self.get_free_energy_and_error()[0]
        self.dG_error = self.get_free_energy_and_error()[1]

    # gather the simulation output files and ready them for further processing
    def get_files(self, path: str) -> tuple:
        files = tuple(sorted(glob.glob(path)))
        return files

    # extract thermodynamic data from simulation output-file and
    # cut the unequilibrated simulation frames
    def get_dVdl(self) -> Tuple[pd.DataFrame, pd.DataFrame]:
        df = alchemlyb.concat(
            [amber.extract_dHdl(file, T=self.temp) for file in self.files]
        )
        dataframe = alchemlyb.concat(
            [d[self.cut_non_eq :] for _, d in df.groupby("lambdas")]
        ).reset_index()
        dataframe = dataframe.set_index(["time", "lambdas"], append=True)
        dataframe.index.names = ["time", "old_time", "lambdas"]
        dataframe = dataframe.droplevel("old_time")
        try:
            decorrelated_dataframe = slicing(dataframe, step=100, force=False)
        except IndexError:
            print("not enough dVdl values to perform subsampling/decorrelation")
        return decorrelated_dataframe

    # fit the data (and extract it too in the case of mbar)
    def get_TI(self) -> pd.DataFrame:
        return TI().fit(self.dVdl_kcal)

    # making MBAR a class property does the same as initializing self.MBAR in __init__()
    # but makes the u_nk parsing etc. only happen if self.MBAR is called, otherwise
    # it will be left uninitialized / self._MBAR initialized to None as per __init__()
    @property
    def MBAR(self):
        if self._MBAR is None:
            self._MBAR = self.get_MBAR()
        return self._MBAR

    def get_MBAR(self) -> pd.DataFrame:
        df = alchemlyb.concat(
            [amber.extract_u_nk(file, T=self.temp) for file in self.files]
        )
        dataframe = alchemlyb.concat(
            [d[self.cut_non_eq :] for _, d in df.groupby("lambdas")]
        ).reset_index()
        dataframe = dataframe.set_index(["time", "lambdas"], append=True)
        dataframe.index.names = ["time", "old_time", "lambdas"]
        dataframe = dataframe.droplevel("old_time")
        try:
            decorrelated_dataframe = slicing(dataframe, step=100, force=False)
        except IndexError:
            print("not enough u_nk values to perform subsampling/decorrelation")
        return AutoMBAR().fit(decorrelated_dataframe)

    # Free energy estimate (and error) based on TI
    def get_free_energy_and_error(self) -> tuple:
        free_energy = self.TI.delta_f_.iloc[0, -1]
        free_energy_error = self.TI.d_delta_f_.iloc[0, -1]
        return (free_energy, free_energy_error)

    ### plotting ###

    sns.set_context("notebook", font_scale=2)

    def dVdl_per_lambda_plot(self) -> None:
        # set custom seaborn plot style
        palette = sns.color_palette("flare")
        sns.set_theme(style="ticks")
        sns.set_context("notebook", font_scale=2)

        sns.relplot(
            data=self.dVdl_kcal,
            x="time",
            y="dHdl",
            hue="lambdas",
            kind="line",
            palette=palette,
            height=8,
            aspect=1.5,
        )
        plt.ylabel(r"dVdl / $kcal \cdot mol^{-1}$")
        plt.xlabel("frame")

        plt.show()
        None

    def convergence_plot(self) -> None:
        fig, ax = plt.subplots(figsize=(13, 8))
        df = forward_backward_convergence(
                [df for _, df in self.dVdl_kcal.groupby("lambdas")], "TI"
        )
        plot_convergence(df, units="kcal/mol", ax=ax)
        ax.legend(["Error", "Forward", "Reverse"], loc=1)
        plt.show()
        None

    def overlap_matrix(self) -> None:
        fig = plt.figure()
        ax = plot_mbar_overlap_matrix(self.MBAR.overlap_matrix)
        plt.tight_layout()
        plt.show()
        None

    def ti_dHdl(self) -> None:
        fig = plt.figure()
        ax = plot_ti_dhdl([self.TI])
        plt.show()
        None

    def compare_estimators(self) -> None:
        fig = plt.figure()
        ax = plot_dF_state(
            [self.TI, self.MBAR], colors=["#F97306", "#7BC8F6"], orientation="landscape"
        )


# wrapper function to get_free_energy_and_error to print the free energy of the individual calculation step
def get_FE(FEs: List[float], FE_errors: List[float]) -> None:
    display(Markdown("<br>"))
    display(
        Markdown(
            f"{FEs[0]:.4f} = $\Delta\Delta G$, {FE_errors[0]:.4f} = $\Delta\Delta G$ error"
        )
    )
    display(
        Markdown(
            f"{FEs[1]:.4f} = $\Delta\Delta G$, {FE_errors[1]:.4f} = $\Delta\Delta G$ error"
        )
    )
    if len(FEs) == 3:
        display(
            Markdown(
                f"{FEs[2]:.4f} = $\Delta\Delta G$, {FE_errors[2]:.4f} = $\Delta\Delta G$ error"
            )
        )
        display(Markdown("<br>"))


# get the free energy of the monomer/dimer form the dc/vdw/rc steps (-> len(FEs) == 3)
# get the free energy of ligand binding form ligands/complex steps (-> len(FEs) == 2)
def system_delta_G(FEs: List[float], FE_errors: List[float]) -> tuple:
    if len(FEs) == 3:
        ddG = FEs[0] + FEs[1] + FEs[2]
        ddG_error = math.sqrt(FE_errors[0] ** 2 + FE_errors[1] ** 2 + FE_errors[2] ** 2)

    if len(FEs) == 2:
        ddG = FEs[0] + FEs[1]
        ddG_error = math.sqrt(FE_errors[0] ** 2 + FE_errors[1] ** 2)

    display(Markdown(f"{ddG:.4f} = $\Delta\Delta G$"))
    display(Markdown(f"{ddG_error:.4f} = $\Delta\Delta G$ error"))
    return (ddG, ddG_error)


### antibody specific ###

# get the free energy of formation from monomer/dimer free energies
def antibody_delta_G(
    FEs: List[float],
    FE_errors: List[float],
    T_sim: float,
    FRET_probs: List[float] = [],
    UPLC_probs: List[float] = [],
) -> None:
    monA, monB = FEs[0], FEs[1]
    ddG_AA = FEs[2] - (monA + monA)
    ddG_AB = FEs[3] - (monA + monB)
    ddG_BB = FEs[4] - (monB + monB)
    ddGs = {"AA": ddG_AA, "AB": ddG_AB, "BB": ddG_BB}

    monA_err, monB_err = FE_errors[0], FE_errors[1]
    ddG_AA_error = math.sqrt((FE_errors[2] ** 2) + (monA_err) ** 2 + (monB_err) ** 2)
    ddG_AB_error = math.sqrt((FE_errors[3] ** 2) + (monA_err) ** 2 + (monB_err) ** 2)
    ddG_BB_error = math.sqrt((FE_errors[4] ** 2) + (monA_err) ** 2 + (monB_err) ** 2)

    display(Markdown("<br>"))
    display(
        Markdown(
            f"$\Delta\Delta$G dimer AA = {ddG_AA:.5f} $\pm$ {ddG_AA_error:.5f} kcal/mol"
        )
    )
    display(
        Markdown(
            f"$\Delta\Delta$G dimer AB = {ddG_AB:.5f} $\pm$ {ddG_AB_error:.5f} kcal/mol"
        )
    )
    display(
        Markdown(
            f"$\Delta\Delta$G dimer BB = {ddG_BB:.5f} $\pm$ {ddG_BB_error:.5f} kcal/mol"
        )
    )

    _min_sys, _min_val = [(k, v) for k, v in ddGs.items() if v == min(ddGs.values())][0]
    display(Markdown(f"lowest energy dimer: {_min_sys} with {_min_val:.5f} kcal/mol"))

    display(Markdown("----------------"))
    display(Markdown("<br>"))
    display(Markdown("$\Delta\Delta$G reaction"))
    display(Markdown(r"${AA + BB  -> 2AB}$"))
    display(Markdown("<br>"))

    ddG_reaction_calculated = 2 * ddG_AB - ddG_AA - ddG_BB
    display(
        Markdown(f"$\Delta\Delta$G calculated: {ddG_reaction_calculated:.5f} kcal/mol")
    )

    # convert the gas constant (J/molK) to kcal/molK
    R_kcal = R / 4184

    # calculate the reaction rate from experimental dimer probabilities
    if FRET_probs != []:
        FRET_prob_AA, FRET_prob_AB, FRET_prob_BB = (
            FRET_probs[0],
            FRET_probs[1],
            FRET_probs[2],
        )
        FRET_K = (FRET_prob_AB**2) / (FRET_prob_AA * FRET_prob_BB)
        ddG_reaction_FRET = -R_kcal * T_sim * math.log(FRET_K, math.e)
        display(Markdown(f"$\Delta\Delta$G FRET: {ddG_reaction_FRET:.5f} kcal/mol"))

    if UPLC_probs != []:
        UPLC_prob_AA, UPLC_prob_AB, UPLC_prob_BB = (
            UPLC_probs[0],
            UPLC_probs[1],
            UPLC_probs[2],
        )
        UPLC_K = (UPLC_prob_AB**2) / (UPLC_prob_AA * UPLC_prob_BB)
        ddG_reaction_UPLC = -R_kcal * T_sim * math.log(UPLC_K, math.e)
        display(Markdown(f"$\Delta\Delta$G UPLC: {ddG_reaction_UPLC:.5f} kcal/mol"))


### ligands specific ###

# work in progress / untested
# def FE_comparison_plot(calculated_FEs: [float], calculated_FE_errors: [float],
#         experimental_FEs: [float], experimental_FE_errors: [float], paper_FEs=[], paper_FE_errors=[])) -> None:
#     """ Keep in mind, that the first value in the lists passed will be assumed to be the reference system """
#
# def calculate_relative_FE_from_absolute_FE_values_ex_or_calculated(dG_list):
#     dG_normalized = []
#     for i in dG_list:
#         dG_normalized.append(i - dG_list[0])
#     return dG_normalized
#
#
# def calculate_the_mean_error_of_FE_errors(errors=[]):
#     def additive_error_propagation_formula(a,b):
#         return math.sqrt((a**2) + (b)**2)
#     mean_error = []
#     for i in range(len(errors)-1):
#         mean_error.append(additive_error_propagation_formula(errors[i], errors[i+1]))
#     return mean_error
#     None
