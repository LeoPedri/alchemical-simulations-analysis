#!/bin/bash
#
# Setup for the free energy calculatoions: creates and copies as necessary.

basedir=/media/storage_6/lpr/5DJ8/run2/prod
top=$(pwd)

for system in monomerA monomerB double_mut_AA double_mut_AB double_mut_BB;
do

  cd $system
  for charge in dc rc vdw+bonded;
  do

    cd $charge
    genremdinputs.py -inputs hamiltonians_*.dat -groupfile groupfile.ref -i prod_HREX*.tmpl
    cd ..

  done
  cd ..

done
cd $top
