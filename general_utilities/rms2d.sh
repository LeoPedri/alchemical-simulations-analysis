#!/bin/bash
# calculate the rms2d of each trajectory in the usual directory structure

top=$(pwd)

for system in monomerA monomerB double_mut_AA double_mut_AB double_mut_BB;
do
    if [ \! -d $system ]; then
        mkdir $system
    fi

    cd $system
    for charge in dc rc vdw+bonded;
    do
        if [ \! -d $charge ]; then
            mkdir $charge
        fi

        cd $charge
        if [ $charge = vdw+bonded ]; then
            for i in $(seq -f "%03g" 1 12);
            do
                cpptraj.OMP <<EOF
parm ti.parm7
trajin ti.${i}.nc 1 -1 10
strip :WAT
autoimage
rms first @CA 
rms2d @CA out 2drms_${i}.gnu

EOF
            done
        else
            for i in $(seq -f "%03g" 1 6);
            do 
                cpptraj.OMP <<EOF
parm ti.parm7
trajin ti.${i}.nc 1 -1 10
strip :WAT
autoimage
rms first @CA 
rms2d @CA out 2drms_${i}.gnu

EOF
            done
        fi
        cd ..

    done
    cd ..

done
cd $top
