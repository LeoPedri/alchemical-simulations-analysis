#!/bin/bash


# requirements: python 3.8 >= version >= 3.6, amber/20_tools...
# for dependencies see github

top=$(pwd)

if [ \! -d OCD_angles ]; then
    mkdir OCD_angles
fi
cd OCD_angles

for system in double_mut_AA double_mut_AB double_mut_BB;
do

    if [ \! -d $system ]; then
        mkdir ${system}
    fi
    cd $system

    for charge in dc rc vdw+bonded;
    do
        
        if [ \! -d $charge ]; then
            mkdir ${charge}
        fi
        cd $charge

        if [ $charge = vdw+bonded ];
        then
            for i in $(seq -f "%03g" 1 12);
            do 
                ln -s ${top}/${system}/${charge}/ti.parm7 .
                ln -s ${top}/${system}/${charge}/ti.${i}.nc .
                # remove ti masks, caps and sidechain to make it comparable to reference
                cpptraj.OMP <<EOF
parm ti.parm7
trajin ti.${i}.nc 1 -1 10
strip ':107,108,215,216'
strip ':NME,ACE'
strip '!@CA,C,O,N,H'
strip :WAT parmout ${system}_${charge}.parm7
autoimage
rms first @CA 
trajout ${system}_${charge}_${i}.nc

EOF

                OCD -i ${system}_${charge}_${i}.nc\
                    -t ${system}_${charge}.parm7\
                    -A ":1-105" -B ":107-213"\
                    -r /media/storage_6/lpr/5DJ8/OCD_angles_xray_structures/leap/3AVE/dry3AVExray.pdb\
                    --pymol --plot -o ${system}_${charge}_${i}
            done
        else 
            for i in $(seq -f "%03g" 1 6);
            do 
                ln -s ${top}/${system}/${charge}/ti.parm7 .
                ln -s ${top}/${system}/${charge}/ti.${i}.nc .
                # remove ti masks, caps and sidechain to make it comparable to reference
                cpptraj.OMP <<EOF
parm ti.parm7
trajin ti.${i}.nc 1 -1 10
strip ':107,108,215,216'
strip ':NME,ACE'
strip '!@CA,C,O,N,H'
strip :WAT parmout ${system}_${charge}.parm7
autoimage
rms first @CA 
trajout ${system}_${charge}_${i}.nc

EOF

                OCD -i ${system}_${charge}_${i}.nc\
                    -t ${system}_${charge}.parm7\
                    -A ":1-105" -B ":107-213"\
                    -r /media/storage_6/lpr/5DJ8/OCD_angles_xray_structures/leap/3AVE/dry3AVExray.pdb\
                    --pymol --plot -o ${system}_${charge}_${i}
            done
        fi

        cd ..

    done
    cd ..

done
cd $top

