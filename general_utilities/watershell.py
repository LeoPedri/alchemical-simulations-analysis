#!/usr/bin/env python3

import argparse
import os
import re
import subprocess
import sys
import numpy as np
import pandas as pd
import matplotlib as mpl
from matplotlib import pyplot as plt
import seaborn as sns
from matplotlib.ticker import FuncFormatter


####################
##### plotting #####
####################


# histogram available but still very sloppy (work in progress)
def generate_histogram(watershell_output_file_name: str) -> None:
    watershell_dataframe = pd.read_table(
        watershell_output_file_name, delim_whitespace=True
    )
    sim_len = len(watershell_dataframe)

    watershell_dataframe.columns = [
        "frames",
        "first_water_shell_waters",
        "second_water_shell_waters",
    ]
    watershell_dataframe.drop(labels="frames", axis=1)
    watershell_dataframe = pd.DataFrame(
        pd.concat(
            [
                watershell_dataframe.first_water_shell_waters,
                watershell_dataframe.second_water_shell_waters,
            ],
            axis=0,
            ignore_index=True,
        )
    )
    watershell_dataframe = pd.DataFrame(watershell_dataframe)
    first_water_shell_label_array = np.array(
        ["first_water_shell" for _ in range(sim_len)]
    )
    second_water_shell_label_array = np.array(
        ["second_water_shell" for _ in range(sim_len)]
    )
    new_col = np.concatenate(
        (first_water_shell_label_array, second_water_shell_label_array)
    )
    watershell_dataframe["water_shell"] = new_col
    watershell_dataframe.columns = ["water_count", "water_shell"]

    fig, ax = plt.subplots()
    sns.despine(fig)
    sns.histplot(
        watershell_dataframe,
        x=watershell_dataframe.water_count,
        hue=watershell_dataframe.water_shell,
        multiple="stack",
        palette=["aqua", "whitesmoke"],
        edgecolor=".3",
        linewidth=0.5,
        log_scale=False,
        ax=ax,
    )
    plt.axhline(
        y=sim_len,
        xmin=0,
        xmax=1,
        alpha=0.6,
        linewidth=0.8,
        linestyle="--",
        color="grey",
    )
    ax.set_xlim(left=0.5, right=None)
    ax.set_xlabel("water molecules count")
    ax.set_ylabel("frame count")
    ax.legend(["total frame count", "second water shell", "first water shell"])
    plt.savefig(f"{output_file_name}.png", dpi=500, bbox_inches="tight")


def generate_timeseries_plot(
    watershell_output_file_name: str, simulation_length: int = 0
) -> None:
    watershell_dataframe = pd.read_table(
        watershell_output_file_name, delim_whitespace=True
    )

    watershell_dataframe.columns = ["Frame", "first shell", "second shell"]
    xticks = watershell_dataframe["Frame"]
    first_shell_waters = watershell_dataframe["first shell"].to_numpy()
    second_shell_waters = watershell_dataframe["second shell"].to_numpy()
    bin_width = 0.8
    if simulation_length != 0:
        conversion_factor = 1 / (
            round((xticks.max() / (np.int64(simulation_length))), 0)
        )
        converter = lambda x: (x * conversion_factor)
        xticks = watershell_dataframe["Frame"].apply(converter)
        bin_width = converter(bin_width)

    fig, ax = plt.subplots()
    sns.despine()
    sns.set_context("poster")

    colors = [args.color1, args.color2]
    cb_ax = fig.add_axes([0.94, 0.1, 0.04, 0.775])
    cmap = mpl.colors.ListedColormap(colors)
    colorbar = mpl.colorbar.ColorbarBase(
        ax=cb_ax,
        cmap=cmap,
        orientation="vertical",
        ticks=[0.25, 0.75],
        spacing="uniform",
    )
    colorbar.set_ticklabels(["r=3.4 Å", "r=5.0 Å"])
    colorbar.set_label("cutoff radius")

    if len(xticks) <= 1_000:
        first_shell_rolling_average = (
            watershell_dataframe["first shell"].rolling(30).mean()
        )
        second_shell_rolling_average = (
            watershell_dataframe["second shell"].rolling(30).mean()
        )
    elif len(xticks) > 1_000 and len(xticks) <= 2_500:
        first_shell_rolling_average = (
            watershell_dataframe["first shell"].rolling(100).mean()
        )
        second_shell_rolling_average = (
            watershell_dataframe["second shell"].rolling(100).mean()
        )
    else:
        first_shell_rolling_average = (
            watershell_dataframe["first shell"].rolling(150).mean()
        )
        second_shell_rolling_average = (
            watershell_dataframe["second shell"].rolling(150).mean()
        )

    # draw the second solvation shell first, as it would otherwise overshadow the first in the plot
    ax.bar(x=xticks, height=second_shell_waters, width=bin_width, color=colors[1])
    ax.bar(x=xticks, height=first_shell_waters, width=bin_width, color=colors[0])
    ax.plot(
        xticks,
        first_shell_rolling_average,
        linewidth=1,
        color="red",
        label="first shell rolling avg",
    )
    ax.plot(
        xticks,
        second_shell_rolling_average,
        linewidth=1,
        color="slategray",
        label="second shell rolling avg",
    )

    ax.set_ylim([0, second_shell_waters.max() + 1])
    ax.set_xlim([0, xticks.max() + xticks.max() * 0.05])
    # eliminate .5 ticks as there are no half waters
    if (second_shell_waters.max() + 1) <= 10:
        ax.set_yticks(np.arange(0, second_shell_waters.max() + 1, step=1, dtype=int))
    elif (second_shell_waters.max() + 1) <= 20:
        ax.set_yticks(np.arange(0, second_shell_waters.max() + 1, step=2, dtype=int))
    else:
        ax.set_yticks(np.arange(0, second_shell_waters.max() + 1, step=5, dtype=int))

    ax.set_ylabel("water molecules around solute mask")
    if simulation_length != 0:
        if int(simulation_length) > 1_000:
            ax.set_xlabel("time / μs")
            ax.xaxis.set_major_formatter(
                FuncFormatter(lambda x, pos: f"{x / 1_000 :.2f}")
            )
        ax.set_xlabel("time / ns")
    else:
        ax.set_xlabel("Frame")

    plt.savefig(f"{output_file_name}.png", dpi=500, bbox_inches="tight")


######################
##### CLI parser #####
######################

parser = argparse.ArgumentParser(
    prog="watershell.py",
    description="Count the number of water molecules in the first and second solvation shell"
    "using cpptraj's (watershell command) and plot a timeseries and or histogram of the data.",
)

parser.add_argument(
    "-O",
    dest="overwrite",
    required=False,
    action="store_true",
    help="set this flag if you want to overwrite existing files with the same name",
)

parser.add_argument(
    "-top", "-topology", dest="topology", required=True, help="/path/to/topology"
)

parser.add_argument(
    "-traj",
    "-trajectory",
    dest="trajectory",
    required=True,
    help='"/path/to/trajectory [start stop stride]"\n'
    'optional paramters in [], e.g. -traj 001.nc or also -traj "001.nc 1 -1 10")',
)

parser.add_argument(
    "-m",
    "-mask",
    dest="mask",
    required=True,
    help="cpptraj atom/residue selection mask\n"
    "e.g. :15-30 for residues 15 to 30 or @12,17 for atoms 12 and 17.\n"
    "If only numbers are passed e.g. 60,68 the selection will default to "
    "considering them as residues i.e. :60,68",
)

parser.add_argument(
    "-sl",
    "-simulation_length",
    dest="simulation_length",
    required=False,
    default=0,
    help="length of the input trajectory\n in nanoseconds" "e.g. -sl 100",
)

parser.add_argument(
    "-r1",
    "-first_shell_radius",
    dest="first_shell_radius",
    required=False,
    default=3.4,
    help="Radius of the first solvation shell in Å (default is 3.4)",
)

parser.add_argument(
    "-r2",
    "-second_shell_radius",
    dest="second_shell_radius",
    required=False,
    default=5.0,
    help="Radius of the second solvation shell in Å (default is 5.0)",
)

parser.add_argument(
    "-n",
    "-name",
    dest="name",
    required=False,
    default="",
    help="prefix for the output file e.g. {prefix}_watershell_trajectory.dat\n"
    'default = "" i.e. the program output will be watershell_trajectory.dat,'
    "where trajectory is the name of the input traj",
)

parser.add_argument(
    "-hist",
    "-histogram",
    dest="histogram",
    required=False,
    action="store_true",
    help="set this flag if you want to produce/save the histogram plot\n"
    "by default only the timeseries will be produced/saved",
)

parser.add_argument(
    "-ts",
    "-timeseries",
    dest="timeseries",
    required=False,
    default="True",
    help="set this flag to False if you don't want to produce/save the timeseries plot\n"
    "by default both the histogram and the timeseries will be produced/saved",
)

parser.add_argument(
    "-c1",
    "-color1",
    dest="color1",
    required=False,
    default="navy",
    help="Matplotlib color for the resulting plot(s)",
)

parser.add_argument(
    "-c2",
    "-color2",
    dest="color2",
    required=False,
    default="turquoise",
    help="Matplotlib color for the resulting plot(s)",
)

args = parser.parse_args()

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(1)

# extract the trajectory path and separate it from the name
# useful if watershell.py is run from a different directory as the
# trajectories and that directory is read-only for the user
path = args.trajectory.split("/")[:-1]
if len(path) > 1:
    path = "/".join(path)
    path += "/"
else:
    path = ""
# extract only the trajectory name if start/stop/stride is passed
# with the trajectory as an argument
trajectory_name = re.match(r"\A\S+", args.trajectory.split("/")[-1])
args.trajectory = trajectory_name.group()

if args.name != "":
    output_file_name = f"{args.name}_watershell_{args.trajectory}"
else:
    output_file_name = f"watershell_{args.trajectory}"

if (args.mask.startswith(":")) or (args.mask.startswith("@")):
    pass
else:
    args.mask = ":" + args.mask


if (
    args.overwrite
    or subprocess.run(
        [f"ls {output_file_name}.dat /dev/null"], shell=True, capture_output=True
    ).returncode
    != 0
):
    os.system(
        f"""cpptraj.OMP <<EOF
parm {args.topology}
trajin {path+trajectory_name.string}
watershell '{args.mask}' out {output_file_name}.dat \
lower {args.first_shell_radius} upper {args.second_shell_radius}

EOF"""
    )
else:
    print(
        "Warning: skipping cpptraj watershell computation and directly updating/generating the plot(s)"
    )


if args.histogram:
    generate_histogram(f"{output_file_name}.dat")

if args.timeseries:
    generate_timeseries_plot(f"{output_file_name}.dat", args.simulation_length)
