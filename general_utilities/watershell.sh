#!/bin/bash

top=$(pwd)


for node in "double_mut_AA" "double_mut_AB" "double_mut_BB"
do
    cd $node


    cd "dc"
    for i in $(seq -f "%03g" 1 6)
    do
        $top/watershell.py -top ti.parm7 -traj "ti.${i}.nc 1 -1 10" -mask :68,176
    done
    cd ..

    cd "vdw+bonded"
    for i in $(seq -f "%03g" 1 12)
    do
        $top/watershell.py -top ti.parm7 -traj "ti.${i}.nc 1 -1 10" -mask :68,176
    done
    cd ..

    cd "rc"
    for i in $(seq -f "%03g" 1 6)
    do
        $top/watershell.py -top ti.parm7 -traj "ti.${i}.nc 1 -1 10" -mask :68,176
    done
    cd ..


    cd ..
done
