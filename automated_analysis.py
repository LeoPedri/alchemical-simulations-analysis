# mypy typechecking modules
from typing import List, Tuple

# general modules
import glob
# import logging
import math
import matplotlib.pyplot as plt
import pandas as pd
from pandas.plotting import autocorrelation_plot, bootstrap_plot
import seaborn as sns
from scipy.constants import R
from IPython.display import display, Markdown

# alchemlyb parsing, preprocessing and FE estimators
import alchemlyb
from alchemlyb.parsing.amber import extract
from alchemlyb.postprocessors.units import to_kcalmol
from alchemlyb.preprocessing.subsampling import (
    decorrelate_u_nk,
    slicing,
    decorrelate_dhdl,
)
from alchemlyb.estimators import TI, MBAR, AutoMBAR

# alchemlyb plotting
from alchemlyb.convergence import forward_backward_convergence
from alchemlyb.visualisation import (
    plot_convergence,
    plot_mbar_overlap_matrix,
    plot_ti_dhdl,
)
from alchemlyb.visualisation.dF_state import plot_dF_state


class AlchemicalAnalysis:
    def __init__(
        self,
        path: str,
        Temp: float,
        auto_equil: bool = True,
        auto_decorrelate: bool = True,
        cut_non_eq: float = 0,
        cut_from_end: float = 1e9,
        subsample: int = 0,
    ):
        """
        data will be extracted in the interval [cut_non_eq, cut_from_end] (<- mathematical notation)
        these variables are given in picoseconds!
        """
        self.path = path
        self.temp = Temp
        self.auto_equil = auto_equil
        self.auto_decorrelate = auto_decorrelate
        self.cut_non_eq = cut_non_eq
        self.cut_from_end = cut_from_end
        self.subsample = subsample
        self.thermodynamic_data = self.extract_dVdl_unk()
        self.dVdl_kcalmol = self.thermodynamic_data[0]
        self.u_nk_kcalmol = self.thermodynamic_data[1]
        self.dVdl_kcalmol_list = self.thermodynamic_data[2]
        self.u_nk_kcalmol_list = self.thermodynamic_data[3]
        self.FE_TI = self.fe_ti()
        self.FE_MBAR = self.fe_mbar()

    # --- Parsing, preprocessing datasets and estimating FEs --- #

    def extract_dVdl_unk(self):
        """Using auto_equil=True in the __init__ method (i.e. when initializing the class),
        automatic equilibration phase detection (and removal with remove_burnin=True)
        will be performed in addition to decorrelation. - computationally very slow

        Using auto_equil=False in the __init__ method, only decorrelation will be performed. The cut_non_eq variable
        gives the option to remove data (even if decorrelated) beginning at the start of the simulation. The unit of
        time to cut/discard is picoseconds! (i.e. cut_non_eq=10_000 would cut the first 10 nanoseconds of simulation
        time from each lambda window) - computationally fast

        If auto_equil and auto_decorrelate are set to false in the __init__ method, manual equilibration time removal
        with cut_non_eq and manual decorrelation with subsampling (i.e. using only every nth frame as specified with
        subsample) is performed."""

        # extract dVdl & unk form simulation mdouts
        td_data_list = [
            extract(file, T=self.temp) for file in sorted(glob.glob(self.path))
        ]
        _dhdl_per_lw = []
        _u_nk_per_lw = []

        # decorrelate the individual dataframes of the extracted potentials and add them to a list
        for i in range(len(td_data_list)):
            # convert the extracted dataframes from kT to kcal/mol
            td_data_list[i]["dHdl"] = to_kcalmol(td_data_list[i]["dHdl"], self.temp)
            td_data_list[i]["u_nk"] = to_kcalmol(td_data_list[i]["u_nk"], self.temp)

            if self.auto_equil is True and self.auto_decorrelate is True:
                # important note: these two lines are a time-intensive bottleneck!
                _dhdl_per_lw.append(
                    decorrelate_dhdl(
                        slicing(
                            td_data_list[i]["dHdl"],
                            lower=self.cut_non_eq,
                            upper=self.cut_from_end,
                        ),
                        remove_burnin=True,
                    )
                )
                _u_nk_per_lw.append(
                    decorrelate_u_nk(
                        slicing(
                            td_data_list[i]["u_nk"],
                            lower=self.cut_non_eq,
                            upper=self.cut_from_end,
                        ),
                        remove_burnin=True,
                    )
                )
            elif self.auto_equil is False and self.auto_decorrelate is True:
                _dhdl_per_lw.append(
                    decorrelate_dhdl(
                        slicing(
                            td_data_list[i]["dHdl"],
                            lower=self.cut_non_eq,
                            upper=self.cut_from_end,
                        ),
                        remove_burnin=False,
                    )
                )
                _u_nk_per_lw.append(
                    decorrelate_u_nk(
                        slicing(
                            td_data_list[i]["u_nk"],
                            lower=self.cut_non_eq,
                            upper=self.cut_from_end,
                        ),
                        remove_burnin=False,
                    )
                )
            else:
                _dhdl_per_lw.append(
                    slicing(
                        td_data_list[i]["dHdl"],
                        lower=self.cut_non_eq,
                        upper=self.cut_from_end,
                        step=self.subsample,
                    )
                )
                _u_nk_per_lw.append(
                    slicing(
                        td_data_list[i]["u_nk"],
                        lower=self.cut_non_eq,
                        upper=self.cut_from_end,
                        step=self.subsample,
                    )
                )

        # store the list of dataframes (needed for plots later)
        dhdl_list = _dhdl_per_lw
        u_nk_list = _u_nk_per_lw

        # combine the pruned and decorrelated dataframes in the list and convert units
        dhdl = alchemlyb.concat(_dhdl_per_lw)
        u_nk = alchemlyb.concat(_u_nk_per_lw)

        return dhdl, u_nk, dhdl_list, u_nk_list

    # feed the decorrelated dV/dl dataframes to the TI estimator
    def fe_ti(self) -> dict:
        ti = TI().fit(self.dVdl_kcalmol)
        return {
            "TI-FE estimate": ti.delta_f_.loc[0.00, 1.00],
            "estimate uncertainty": ti.d_delta_f_.loc[0.00, 1.00],
        }

    # feed the decorrelated u_nk dataframes to the MBAR estimator
    def fe_mbar(self) -> dict:
        try:
            mbar = AutoMBAR().fit(self.u_nk_kcalmol)
        except "Failed to use AutoMBAR module, reverting to MBAR module":
            mbar = MBAR().fit(self.u_nk_kcalmol)

        return {
            "MBAR-FE estimate": mbar.delta_f_.loc[0.00, 1.00],
            "estimate uncertainty": mbar.d_delta_f_.loc[0.00, 1.00],
        }

    # --- Plotting step-intrinsic (i.e. convergence & overlap_matrix) --- #
    # sns.set_context("poster")

    def convergence(self, save: str = "") -> None:
        fig, ax = plt.subplots(
            nrows=1, ncols=2, sharex=True, sharey=True, figsize=(13, 6)
        )
        plot_convergence(
            forward_backward_convergence(self.dVdl_kcalmol_list, "TI"),
            units="kcal/mol",
            ax=ax[0],
        )
        plot_convergence(
            forward_backward_convergence(self.u_nk_kcalmol_list, "MBAR"),
            units="kcal/mol",
            ax=ax[1],
        )

        ax[0].set_title("TI")
        ax[1].set_title("MBAR")
        fig.tight_layout()

        if save != "":
            plt.savefig(f"{save}", bbox_inches="tight")

        plt.show()

    def mbar_overlap_matrix(self, save: str = "") -> None:
        fig, ax = plt.subplots()
        plot_mbar_overlap_matrix(
            AutoMBAR().fit(self.u_nk_kcalmol).overlap_matrix, ax=ax
        )
        fig.tight_layout()

        if save != "":
            plt.savefig(f"{save}", bbox_inches="tight")
        plt.show()


# --- Plotting functions to compare between steps --- #


def correlogram(dhdl_series: pd.Series) -> None:
    autocorrelation_plot(dhdl_series)


def bootstrap_plots(dhdl_series: pd.Series) -> None:
    bootstrap_plot(dhdl_series)


def dhdl_histogram(dhdl_data: List, save: str = "") -> None:

    with plt.style.context("default"):
        sns.set_context("notebook")

        fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(18, 5))
        plt.suptitle(
            r"$\dfrac{\partial U}{\partial \lambda}$ histogram overlap of the windows in each step",
            fontsize="xx-large",
            y=1.1,
        )

        titles = {0: "decharging", 1: "van der Waals", 2: "recharging"}
        for index, df in enumerate(dhdl_data):
            sns.histplot(data=df, x="dHdl", hue="lambdas", bins=100, ax=ax[index])
            if index > 0:
                ax[index].set(ylabel=None)
            ax[index].set(
                xlabel=r"$\partial U \cdot \partial \lambda^{-1}$ / $kcal \cdot mol^{-1}$"
            )
            ax[index].set_title(titles[index], fontsize="x-large")

    with plt.style.context("ggplot"):
        sns.set_context("poster")

        fig, ax = plt.subplots(figsize=(10, 7))
        plt.suptitle(
            r"Combined $\frac{\partial U}{\partial \lambda}$ distribution of all steps"
        )

        for step, df in enumerate(dhdl_data):
            if step == 0:
                ax.hist(
                    df["dHdl"],
                    histtype="stepfilled",
                    bins=90,
                    alpha=0.8,
                    density=True,
                    label="dc",
                )
            elif step == 1:
                ax.hist(
                    df["dHdl"],
                    histtype="stepfilled",
                    bins=90,
                    alpha=0.8,
                    density=True,
                    label="vdw",
                )
            elif step == 2:
                ax.hist(
                    df["dHdl"],
                    histtype="stepfilled",
                    bins=90,
                    alpha=0.8,
                    density=True,
                    label="rc",
                )

        ax.set(
            xlabel=r"$\partial U \cdot \partial \lambda^{-1}$ / $kcal \cdot mol^{-1}$",
            ylabel=r"$p_{\partial U / \partial \lambda}$ / -",
        )
        ax.legend()

    if save != "":
        plt.savefig(f"{save}", bbox_inches="tight")

    plt.show()


def ti_dhdl(dhdl_data: List, save: str = "") -> None:
    """dhdl_data must be a litst, where each member is a dataframe (e.g. self.dVdl_kcalmol) of the
    combined and decorrelated data for the respective transformation step (e.g. decharging,...)"""

    if len(dhdl_data) < 1:
        raise Exception("The data input list cannot be empty")

    data = {}
    for i in range(len(dhdl_data)):
        data[i] = TI().fit(dhdl_data[i])

    a, b, c = ("decharging", "VDW", "recharging")
    fig, ax = plt.subplots()
    if len(data) == 1:
        plot_ti_dhdl([data[i] for i in data.keys()], labels=[a], ax=ax)

    elif len(data) == 2:
        plot_ti_dhdl([data[i] for i in data.keys()], labels=[a, b], ax=ax)

    elif len(data) == 3:
        plot_ti_dhdl([data[i] for i in data.keys()], labels=[a, b, c], ax=ax)

    if save != "":
        plt.savefig(f"{save}", bbox_inches="tight")

    plt.show()


def dF_state(td_data: List[Tuple], save: str = "") -> None:
    """dhdl_data must be a litst of tuples, of the form (self.dVdl_kcalmol, self.u_nk_kcalmol), i.e.
    the combined and decorrelated data for the respective transformation step ready to be fed to the
    TI estimator and the MBAR estimator (e.g. decharging,...)"""

    data = {}
    if len(td_data) < 1:
        raise Exception("The data input list cannot be empty")
    elif len(td_data[0]) == 1:
        data["TI"] = TI().fit(td_data[0][0])
        try:
            data["MBAR"] = AutoMBAR().fit(alchemlyb.concat(td_data[0][1]))
        except "Failed to use AutoMBAR module, reverting to MBAR module":
            data["MBAR"] = MBAR().fit(td_data[0][1])

    elif len(td_data[0]) == 2:
        data["TI"] = (TI().fit(td_data[0][0]), TI().fit(td_data[1][0]))
        try:
            data["MBAR"] = (
                AutoMBAR().fit(td_data[0][1]),
                AutoMBAR().fit(td_data[1][1]),
            )
        except "Failed to use AutoMBAR module, reverting back to MBAR module":
            data["MBAR"] = (MBAR().fit(td_data[0][1]), MBAR().fit((td_data[1][1])))

    elif len(td_data[0]) == 3:
        data["TI"] = (
            TI().fit(td_data[0][0]),
            TI().fit(td_data[1][0]),
            TI().fit(td_data[2][0]),
        )
        try:
            data["MBAR"] = (
                AutoMBAR().fit(td_data[0][1]),
                AutoMBAR().fit(td_data[1][1]),
                AutoMBAR().fit(td_data[2][1]),
            )
        except "Failed to use AutoMBAR module, reverting back to MBAR module":
            data["MBAR"] = (
                MBAR().fit(td_data[0][1]),
                MBAR().fit(td_data[1][1]),
                MBAR().fit(td_data[2][1]),
            )

    fig = plot_dF_state(
        [data[i] for i in data.keys()],
        colors=["#F97306", "#7BC8F6"],
        units="kcal/mol",
        orientation="portrait",
    )

    if save != "":
        fig.savefig(f"{save}", bbox_inches="tight")

    fig.show()


# --- Pretty printing free energies --- #


def get_FE(
    FEs_TI: List[float],
    FE_TI_errors: List[float],
    FEs_MBAR: List[float],
    FE_MBAR_errors: List[float],
) -> dict:
    ddG_ti = 0.0
    ddG_mbar = 0.0
    ddG_ti_error = 0.0
    ddG_mbar_error = 0.0
    if len(FEs_TI) < 1 or len(FEs_TI) < 1:
        raise Exception("please provide FE and associated error values")
    else:
        for i in range(len(FEs_TI)):
            ddG_ti += FEs_TI[i]
            ddG_ti_error += FE_TI_errors[i] ** 2
        ddG_ti_error = math.sqrt(ddG_ti_error)
        for i in range(len(FEs_MBAR)):
            ddG_mbar += FEs_MBAR[i]
            ddG_mbar_error += FE_MBAR_errors[i] ** 2
        ddG_mbar_error = math.sqrt(ddG_mbar_error)

    pd.options.display.float_format = "{:,.2f}".format
    df = pd.DataFrame(
        data=[[ddG_ti, ddG_mbar], [ddG_ti_error, ddG_mbar_error]],
        index=["ΔG", "error"],
        columns=["TI [kcal/mol]", "MBAR [kcal/mol]"],
    )
    display(df)

    return {
        "dG TI": ddG_ti,
        "TI error": ddG_ti_error,
        "dG MBAR": ddG_mbar,
        "MBAR error": ddG_mbar_error,
    }


def system_free_energies(
    FEs_TI: List[float],
    FE_TI_errors: List[float],
    FEs_MBAR: List[float],
    FE_MBAR_errors: List[float],
    T_sim: float,
    FRET_probs: List[float] = [],
    UPLC_probs: List[float] = [],
) -> None:

    FEs = {
        "TI": {
            "A": FEs_TI[0],
            "B": FEs_TI[1],
            "AA": FEs_TI[2],
            "AB": FEs_TI[3],
            "BB": FEs_TI[4],
        },
        "MBAR": {
            "A": FEs_MBAR[0],
            "B": FEs_MBAR[1],
            "AA": FEs_MBAR[2],
            "AB": FEs_MBAR[3],
            "BB": FEs_MBAR[4],
        },
    }
    errors = {
        "TI_errors": {
            "A": FE_TI_errors[0],
            "B": FE_TI_errors[1],
            "AA": FE_TI_errors[2],
            "AB": FE_TI_errors[3],
            "BB": FE_TI_errors[4],
        },
        "MBAR_errors": {
            "A": FE_MBAR_errors[0],
            "B": FE_MBAR_errors[1],
            "AA": FE_MBAR_errors[2],
            "AB": FE_MBAR_errors[3],
            "BB": FE_MBAR_errors[4],
        },
    }

    ti_ddG_AA = FEs["TI"]["AA"] - (FEs["TI"]["A"] + FEs["TI"]["A"])
    ti_ddG_AB = FEs["TI"]["AB"] - (FEs["TI"]["A"] + FEs["TI"]["B"])
    ti_ddG_BB = FEs["TI"]["BB"] - (FEs["TI"]["B"] + FEs["TI"]["B"])

    ti_ddG_AA_error = math.sqrt(
        (errors["TI_errors"]["AA"] ** 2)
        + (errors["TI_errors"]["A"]) ** 2
        + (errors["TI_errors"]["A"]) ** 2
    )
    ti_ddG_AB_error = math.sqrt(
        (errors["TI_errors"]["AB"] ** 2)
        + (errors["TI_errors"]["A"]) ** 2
        + (errors["TI_errors"]["B"]) ** 2
    )
    ti_ddG_BB_error = math.sqrt(
        (errors["TI_errors"]["BB"] ** 2)
        + (errors["TI_errors"]["B"]) ** 2
        + (errors["TI_errors"]["B"]) ** 2
    )

    mbar_ddG_AA = FEs["MBAR"]["AA"] - (FEs["MBAR"]["A"] + FEs["MBAR"]["A"])
    mbar_ddG_AB = FEs["MBAR"]["AB"] - (FEs["MBAR"]["A"] + FEs["MBAR"]["B"])
    mbar_ddG_BB = FEs["MBAR"]["BB"] - (FEs["MBAR"]["B"] + FEs["MBAR"]["B"])

    mbar_ddG_AA_error = math.sqrt(
        (errors["MBAR_errors"]["AA"] ** 2)
        + (errors["MBAR_errors"]["A"]) ** 2
        + (errors["MBAR_errors"]["A"]) ** 2
    )
    mbar_ddG_AB_error = math.sqrt(
        (errors["MBAR_errors"]["AB"] ** 2)
        + (errors["MBAR_errors"]["A"]) ** 2
        + (errors["MBAR_errors"]["B"]) ** 2
    )
    mbar_ddG_BB_error = math.sqrt(
        (errors["MBAR_errors"]["BB"] ** 2)
        + (errors["MBAR_errors"]["B"]) ** 2
        + (errors["MBAR_errors"]["B"]) ** 2
    )

    ti_ddG_reaction = 2 * ti_ddG_AB - ti_ddG_AA - ti_ddG_BB
    ti_ddG_reaction_error = math.sqrt(
        ti_ddG_AB_error**2 + ti_ddG_AA_error**2 + ti_ddG_BB_error**2
    )
    mbar_ddG_reaction = 2 * mbar_ddG_AB - mbar_ddG_AA - mbar_ddG_BB
    mbar_ddG_reaction_error = math.sqrt(
        mbar_ddG_AB_error**2 + mbar_ddG_AA_error**2 + mbar_ddG_BB_error**2
    )

    # convert the gas constant (J/molK) to kcal/molK
    R_kcal = R / 4184

    ddG_reaction_FRET = 0
    ddG_reaction_UPLC = 0
    # calculate the reaction rate from experimental dimer probabilities
    if FRET_probs:
        FRET_prob_AA, FRET_prob_AB, FRET_prob_BB = (
            FRET_probs[0],
            FRET_probs[1],
            FRET_probs[2],
        )
        FRET_K = (FRET_prob_AB**2) / (FRET_prob_AA * FRET_prob_BB)
        ddG_reaction_FRET = -R_kcal * T_sim * math.log(FRET_K, math.e)

    if UPLC_probs != []:
        UPLC_prob_AA, UPLC_prob_AB, UPLC_prob_BB = (
            UPLC_probs[0],
            UPLC_probs[1],
            UPLC_probs[2],
        )
        UPLC_K = (UPLC_prob_AB**2) / (UPLC_prob_AA * UPLC_prob_BB)
        ddG_reaction_UPLC = -R_kcal * T_sim * math.log(UPLC_K, math.e)

    display(Markdown("<br>"))
    display(Markdown(r"$Reaction$"))
    display(Markdown(r"${AA + BB  -> 2AB}$"))
    display(Markdown("<br>"))

    # TURN THIS INTO A DATAFRAME
    pd.options.display.float_format = "{:,.2f}".format
    d = {
        "AA": [
            f"{ti_ddG_AA:.2f} $\\pm$ {ti_ddG_AA_error:.2f}",
            f"{mbar_ddG_AA:.2f} $\\pm$ {mbar_ddG_AA_error:.2f}",
            "",
            "",
        ],
        "AB": [
            f"{ti_ddG_AB:.2f} $\\pm$ {ti_ddG_AB_error:.2f}",
            f"{mbar_ddG_AB:.2f} $\\pm$ {mbar_ddG_AB_error:.2f}",
            "",
            "",
        ],
        "BB": [
            f"{ti_ddG_BB:.2f} $\\pm$ {ti_ddG_BB_error:.2f}",
            f"{mbar_ddG_BB:.2f} $\\pm$ {mbar_ddG_BB_error:.2f}",
            "",
            "",
        ],
        "reaction": [
            f"{ti_ddG_reaction:.2f} $\\pm$ {ti_ddG_reaction_error:.2f}",
            f"{mbar_ddG_reaction:.2f} $\\pm$ {mbar_ddG_reaction_error:.2f}",
            f"{ddG_reaction_FRET:.2f}",
            f"{ddG_reaction_UPLC:.2f}",
        ],
    }

    df = pd.DataFrame(
        data=d, index=["TI [kcal/mol]", "MBAR [kcal/mol]", "FRET", "UPLC"]
    )

    display(df)
    display(Markdown("<br>"))
